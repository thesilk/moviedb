package main

import (
	"log"

	"gitlab.com/seide3011/moviedb/app"
)

func main() {
	app := app.App{}
	app.Initialize("movie.db", "./migrations")

	log.Println("Starting server on 0.0.0.0:2342...")
	app.Run("0.0.0.0:2342")
}
