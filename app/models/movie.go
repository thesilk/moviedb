package models

import (
	"database/sql"
	"fmt"

	"gitlab.com/seide3011/moviedb/app/lib"
)

//Movie contains imdb information
type Movie struct {
	ID         int    `json:"-"`
	HashID     string `json:"id"`
	Title      string `json:"title"`
	Year       string `json:"year,omitempty"`
	Rated      string `json:"rated,omitempty"`
	Released   string `json:"released,omitempty"`
	Runtime    string `json:"runtime,omitempty"`
	Genre      string `json:"genre,omitempty"`
	Director   string `json:"director,omitempty"`
	Writer     string `json:"writer,omitempty"`
	Actors     string `json:"actors,omitempty"`
	Plot       string `json:"plot,omitempty"`
	Language   string `json:"language,omitempty"`
	Country    string `json:"country,omitempty"`
	Awards     string `json:"awards,omitempty"`
	Poster     string `json:"poster,omitempty"`
	Metascore  string `json:"metascore,omitempty"`
	ImdbRating string `json:"imdb_rating,omitempty"`
	ImdbVotes  string `json:"imdb_votes,omitempty"`
	ImdbID     string `json:"imdb_id,omitempty"`
	Type       string `json:"type,omitempty"`
	DVD        string `json:"dvd,omitempty"`
	BoxOffice  string `json:"box_office,omitempty"`
	Production string `json:"production,omitempty"`
	Website    string `json:"website,omitempty"`
	Response   string `json:"response,omitempty"`
}

var err error

//GetMovie selects movie with given id in struct
func (m *Movie) GetMovie(db *sql.DB) error {

	hash := lib.NewHashID()
	m.ID, err = hash.DecodeID(m.HashID)
	if err != nil {
		return fmt.Errorf("Couldn't encode id: %v", err)
	}

	err = db.QueryRow("SELECT * FROM movies where id=$1", m.ID).Scan(
		&m.ID,
		&m.Title,
		&m.Year,
		&m.Rated,
		&m.Released,
		&m.Runtime,
		&m.Genre,
		&m.Director,
		&m.Writer,
		&m.Actors,
		&m.Plot,
		&m.Language,
		&m.Country,
		&m.Awards,
		&m.Poster,
		&m.Metascore,
		&m.ImdbRating,
		&m.ImdbVotes,
		&m.ImdbID,
		&m.Type,
		&m.DVD,
		&m.BoxOffice,
		&m.Production,
		&m.Website,
		&m.Response)
	if err != nil {
		return fmt.Errorf("Couldn't get movie entry from db: %v", err)
	}

	return nil

}

//CreateMovie creates movie entry in database from struct
func (m *Movie) CreateMovie(db *sql.DB) error {
	stmt, err := db.Prepare(`
		INSERT INTO
			movies(
				title,
				year,
				rated,
				released,
				runtime,
				genre,
				director,
				writer,
				actors,
				plot,
				language,
				country,
				awards,
				poster,
				metascore,
				imdb_rating,
				imdb_votes,
				imdb_id,
				type,
				dvd,
				box_office,
				production,
				website,
				response
			) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`)

	if err != nil {
		return fmt.Errorf("Couldn't prepare insert statement: %v", err)
	}

	_, err = stmt.Exec(
		&m.Title,
		&m.Year,
		&m.Rated,
		&m.Released,
		&m.Runtime,
		&m.Genre,
		&m.Director,
		&m.Writer,
		&m.Actors,
		&m.Plot,
		&m.Language,
		&m.Country,
		&m.Awards,
		&m.Poster,
		&m.Metascore,
		&m.ImdbRating,
		&m.ImdbVotes,
		&m.ImdbID,
		&m.Type,
		&m.DVD,
		&m.BoxOffice,
		&m.Production,
		&m.Website,
		&m.Response)
	if err != nil {
		return fmt.Errorf("Couldn't execute movie insert statement: %v", err)
	}
	return nil
}

//DeleteMovie deletes given movie
func (m *Movie) DeleteMovie(db *sql.DB) error {
	hash := lib.NewHashID()
	m.ID, err = hash.DecodeID(m.HashID)
	if err != nil {
		return fmt.Errorf("Couldn't encode id: %v", err)
	}

	stmt, err := db.Prepare("DELETE FROM movies where id=?")
	if err != nil {
		return fmt.Errorf("Couldn't pepare statement for deleting movie with id %d: %v", m.ID, err)
	}

	_, err = stmt.Exec(m.ID)
	if err != nil {
		return fmt.Errorf("Couldn't execute query for deleting movie with id %d: %v", m.ID, err)
	}

	return nil
}

//GetMovies returns all movies in from movies table
func GetMovies(db *sql.DB) ([]Movie, error) {
	movies := []Movie{}
	rows, err := db.Query("SELECT * FROM movies ORDER BY title")
	if err != nil {
		return []Movie{}, fmt.Errorf("Couldn't get movies from db: %v", err)
	}
	defer rows.Close()

	for rows.Next() {
		var m Movie
		err := rows.Scan(
			&m.ID,
			&m.Title,
			&m.Year,
			&m.Rated,
			&m.Released,
			&m.Runtime,
			&m.Genre,
			&m.Director,
			&m.Writer,
			&m.Actors,
			&m.Plot,
			&m.Language,
			&m.Country,
			&m.Awards,
			&m.Poster,
			&m.Metascore,
			&m.ImdbRating,
			&m.ImdbVotes,
			&m.ImdbID,
			&m.Type,
			&m.DVD,
			&m.BoxOffice,
			&m.Production,
			&m.Website,
			&m.Response)
		if err != nil {
			return []Movie{}, fmt.Errorf("Couldn't scan movie row: %v", err)
		}

		hash := lib.NewHashID()
		m.HashID, err = hash.EncodeID(m.ID)
		if err != nil {
			return []Movie{}, fmt.Errorf("Couldn't encode id: %v", err)
		}

		movies = append(movies, m)
	}

	return movies, nil
}
