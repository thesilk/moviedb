package controllers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"github.com/gorilla/mux"

	"gitlab.com/seide3011/moviedb/app/models"
)

//GetMovie provides handler function to get specific movie from database
func GetMovie(db *sql.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	m := models.Movie{HashID: vars["id"]}
	if err := m.GetMovie(db); err != nil {
		catchError(w, err)
		return
	}
	respondWithJSON(w, http.StatusOK, m)
}

//DeleteMovie provides handler function to delete specific movie
func DeleteMovie(db *sql.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	m := models.Movie{HashID: vars["id"]}
	if err := m.DeleteMovie(db); err != nil {
		catchError(w, err)
		return
	}
	respondWithJSON(w, http.StatusOK, fmt.Sprintf("Deletion of movie with id %d was successfully!", m.ID))
}

//CreateMovie provides handler function create a new movie db entry
func CreateMovie(db *sql.DB, w http.ResponseWriter, r *http.Request) {
	var movie models.Movie

	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	defer r.Body.Close()

	if err := json.Unmarshal(body, &movie); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	if err := movie.CreateMovie(db); err != nil {
		catchError(w, err)
		return
	}

	respondWithJSON(w, http.StatusOK, "Creating movie database entry was successfully")

}

//GetMovies provides handler function to get all movies from database
func GetMovies(db *sql.DB, w http.ResponseWriter, r *http.Request) {
	movies, err := models.GetMovies(db)
	if err != nil {
		catchError(w, err)
		return
	}
	respondWithJSON(w, http.StatusOK, movies)
}

func catchError(w http.ResponseWriter, err error) {
	switch err {
	case sql.ErrNoRows:
		respondWithError(w, http.StatusNotFound, "No Movies found")
	default:
		respondWithError(w, http.StatusInternalServerError, err.Error())
	}

}
