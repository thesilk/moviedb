package lib

import (
	"fmt"

	"github.com/speps/go-hashids"
)

//HashID struct with salt and minlength
type HashID struct {
	Salt   string
	Length int
}

//NewHashID creates HashID struct with default values
func NewHashID() HashID {
	return HashID{
		Salt:   "avengers",
		Length: 8,
	}
}

func (h *HashID) initHash() (*hashids.HashID, error) {
	hd := hashids.NewData()
	hd.Salt = h.Salt
	hd.MinLength = h.Length

	return hashids.NewWithData(hd)

}

//EncodeID encodes int in hash string
func (h *HashID) EncodeID(id int) (string, error) {
	hd, err := h.initHash()
	if err != nil {
		return "", fmt.Errorf("Couldn't encode id: %v", err)
	}

	return hd.Encode([]int{id})

}

//DecodeID decodes hash in int
func (h *HashID) DecodeID(hashID string) (int, error) {
	hd, err := h.initHash()
	if err != nil {
		return 0, fmt.Errorf("Couldn't encode id: %v", err)
	}
	id, err := hd.DecodeWithError(hashID)

	if len(id) < 1 {
		return 0, fmt.Errorf("couldn't decode hash %s", hashID)
	}
	return id[0], err
}
