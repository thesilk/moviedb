package app

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	//"github.com/mattn/go-sqlite3" needs blank import
	_ "github.com/mattn/go-sqlite3"
	"github.com/pressly/goose"
)

//App handles router and db driver
type App struct {
	Router *mux.Router
	DB     *sql.DB
}

//DATABASEDRIVER sets database technology for goose migrations
const DATABASEDRIVER = "sqlite3"

//Initialize opens database and migrate this
func (a *App) Initialize(dbName, migrations string) {
	var err error
	a.DB, err = sql.Open(DATABASEDRIVER, dbName)
	if err != nil {
		panic(fmt.Sprintf("Couldn't open %s: %v", dbName, err))
	}

	goose.SetDialect(DATABASEDRIVER)
	if err := goose.Up(a.DB, migrations); err != nil {
		panic(fmt.Sprintf("Couldn't migrate database: %v", err))
	}

	a.Router = mux.NewRouter()
	a.initializeRoutes()
}

func (a *App) initializeRoutes() {
	a.Router.HandleFunc("/api/v0/movies", a.getMovies).Methods("GET")
	a.Router.HandleFunc("/api/v0/movie/{id:[a-z,A-Z,0-9]+}", a.getMovie).Methods("GET")
	a.Router.HandleFunc("/api/v0/movie/{id:[a-z,A-Z,0-9]+}", a.deleteMovie).Methods("DELETE")
	a.Router.HandleFunc("/api/v0/movie", a.createMovie).Methods("POST")
}

//Run starts http server
func (a *App) Run(addr string) {
	log.Fatal(http.ListenAndServe(addr, a.Router))
}
