package app_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"gitlab.com/seide3011/moviedb/app"
)

var a app.App

func TestMain(m *testing.M) {
	a = app.App{}

	a.Initialize("test.db", "../migrations")
	deleteTableContent()

	code := m.Run()

	os.Remove("test.db")

	os.Exit(code)
}

func TestEmptyTable(t *testing.T) {
	req, _ := http.NewRequest("GET", "/api/v0/movies", nil)
	resp := executeRequest(req)

	checkResponseCode(t, http.StatusOK, resp.Code)

	if body := resp.Body.String(); body != "[]" {
		t.Errorf("Expected an empty array, got %v", body)
	}
}

func TestGetMovies(t *testing.T) {
	createTestMovieDBEntry()

	req, _ := http.NewRequest("GET", "/api/v0/movies", nil)
	resp := executeRequest(req)

	checkResponseCode(t, http.StatusOK, resp.Code)

	var m []app.Movie
	json.NewDecoder(resp.Body).Decode(&m)

	if len(m) != 1 {
		t.Errorf("Expected an array with one object, got %v", len(m))
	}

	deleteTableContent()
}

func TestGetSpecificMovie(t *testing.T) {
	createTestMovieDBEntry()

	req, _ := http.NewRequest("GET", "/api/v0/movie/laM6zMKD", nil)
	resp := executeRequest(req)

	checkResponseCode(t, http.StatusOK, resp.Code)

	var m app.Movie
	json.NewDecoder(resp.Body).Decode(&m)

	if m.Title != "titanic" {
		t.Errorf("Expected Titanic for ID laM6zMKD, got %s", m.Title)
	}

	deleteTableContent()
}

func TestNonExisitingMovie(t *testing.T) {
	createTestMovieDBEntry()

	req, _ := http.NewRequest("GET", "/api/v0/movie/12", nil)
	resp := executeRequest(req)

	checkResponseCode(t, http.StatusNotFound, resp.Code)

	deleteTableContent()
}

func TestDeleteMovie(t *testing.T) {
	createTestMovieDBEntry()
	req, _ := http.NewRequest("DELETE", "/api/v0/movie/laM6zMKD", nil)
	resp := executeRequest(req)

	checkResponseCode(t, http.StatusOK, resp.Code)

	if body := resp.Body.String(); body == "Deletion of movie with id laM6zMKD was successfully!" {
		t.Errorf("couldn't delete laM6zMKD")
	}

	deleteTableContent()
}

func TestDeleteNonExistingMovie(t *testing.T) {
	createTestMovieDBEntry()
	req, _ := http.NewRequest("DELETE", "/api/v0/movie/123", nil)
	resp := executeRequest(req)

	checkResponseCode(t, http.StatusNotFound, resp.Code)
	deleteTableContent()
}

func TestCreateMovie(t *testing.T) {
	createTestMovieDBEntry()

	payload := []byte(`{"title": "mr robot", "imdb_id": "18" }`)

	req, _ := http.NewRequest("POST", "/api/v0/movie", bytes.NewBuffer(payload))
	resp := executeRequest(req)

	checkResponseCode(t, http.StatusCreated, resp.Code)

	deleteTableContent()
}

func TestCreateMovieWrongJSONFormat(t *testing.T) {
	createTestMovieDBEntry()

	payload := []byte(`{"title": "mr robot", "imdb_id": 18 }`)

	req, _ := http.NewRequest("POST", "/api/v0/movie", bytes.NewBuffer(payload))
	resp := executeRequest(req)

	checkResponseCode(t, http.StatusInternalServerError, resp.Code)

	deleteTableContent()
}

func executeRequest(r *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, r)

	return rr
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected code %d, got %d \n", expected, actual)
	}
}

func deleteTableContent() {
	a.DB.Exec("DELETE FROM movies")
}

func createTestMovieDBEntry() {
	m := app.Movie{
		Title:     "titanic",
		Year:      "1997",
		Rated:     "8.9",
		Released:  "5.8.1997",
		Runtime:   "123",
		Genre:     "Drama",
		Director:  "Cameron",
		Writer:    "Peter",
		Actors:    "DiCaprio",
		Plot:      "...",
		Language:  "deutsch",
		Country:   "DE",
		Awards:    "Oscar",
		Poster:    "dhajsd939e23ndnj",
		Metascore: "8.7",
		ImdbID:    "12",
	}
	m.CreateMovie(a.DB)
}
