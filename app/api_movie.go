package app

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/gorilla/mux"
)

//GetMovie provides handler function to get specific movie from database
func (a *App) getMovie(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	m := Movie{HashID: vars["id"]}
	if err := m.GetMovie(a.DB); err != nil {
		catchError(w, err)
		return
	}
	respondWithJSON(w, http.StatusOK, m)
}

//DeleteMovie provides handler function to delete specific movie
func (a *App) deleteMovie(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	m := Movie{HashID: vars["id"]}
	if err := m.DeleteMovie(a.DB); err != nil {
		catchError(w, err)
		return
	}
	respondWithJSON(w, http.StatusOK, fmt.Sprintf("Deletion of movie with id %s was successfully!", m.HashID))
}

//CreateMovie provides handler function create a new movie db entry
func (a *App) createMovie(w http.ResponseWriter, r *http.Request) {
	var movie Movie

	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	defer r.Body.Close()

	if err := json.Unmarshal(body, &movie); err != nil {
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}

	if err := movie.CreateMovie(a.DB); err != nil {
		catchError(w, err)
		return
	}

	respondWithJSON(w, http.StatusCreated, "Creating movie database entry was successfully")

}

//GetMovies provides handler function to get all movies from database
func (a *App) getMovies(w http.ResponseWriter, r *http.Request) {
	movies, err := GetMovies(a.DB)
	if err != nil {
		catchError(w, err)
		return
	}
	respondWithJSON(w, http.StatusOK, movies)
}

func catchError(w http.ResponseWriter, err error) {
	switch err {
	case sql.ErrNoRows:
		respondWithError(w, http.StatusNotFound, "No Movies found")
	default:
		if strings.Contains(err.Error(), "couldn't decode hash") {
			respondWithError(w, http.StatusNotFound, "couldn't found movie")
		}
		respondWithError(w, http.StatusInternalServerError, err.Error())
	}

}
