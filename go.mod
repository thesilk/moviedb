module gitlab.com/seide3011/moviedb

go 1.12

require (
	github.com/gorilla/mux v1.7.3
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/pressly/goose v2.6.0+incompatible
	github.com/speps/go-hashids v2.0.0+incompatible
)
