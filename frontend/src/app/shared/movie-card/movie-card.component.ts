import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { IOmdbMovie } from '../../movie-search/omdbapi.interface';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss'],
})
export class MovieCardComponent implements OnInit {
  @Input() movieID: string;
  @Input() omdb: boolean;
  @Input() movie: IOmdbMovie;

  constructor(private router: Router) {}

  ngOnInit() {}

  navigateToDetails() {
    const link = this.omdb ? `movie/${this.movieID}` : `search/${this.movieID}`;
    this.router.navigate([link]);
  }

  getTrimmedMovieTitle(): string {
    if (this.movie.Title.length > 30) {
      return `${this.movie.Title.substring(0, 30)}...`;
    }
    return this.movie.Title;
  }
}
