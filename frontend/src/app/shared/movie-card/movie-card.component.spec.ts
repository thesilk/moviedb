import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { By } from '@angular/platform-browser';

import { MovieCardComponent } from './movie-card.component';

describe('MovieCardComponent', () => {
  let component: MovieCardComponent;
  let fixture: ComponentFixture<MovieCardComponent>;
  const mockRouter = { navigate: jasmine.createSpy('navigateToDetails') };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MovieCardComponent],
      providers: [{ provide: Router, useValue: mockRouter }],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovieCardComponent);
    component = fixture.componentInstance;

    component.movieID = 'id0000';
    component.omdb = true;
    component.movie = { Title: 'test', Poster: 'abcd' };

    fixture.detectChanges();
  });

  it('should shorten movie title', () => {
    component.movie = {
      Title: 'abcdefghijklmnopqrstuvwxyz0123456789',
      Poster: 'abcd',
    };
    fixture.detectChanges();

    expect(component.getTrimmedMovieTitle()).toBe(
      'abcdefghijklmnopqrstuvwxyz0123...',
    );
  });

  it('should not shorten movie title', () => {
    component.movie = {
      Title: 'abcdefghijklmnopqrstuvwxyz',
      Poster: 'abcd',
    };
    fixture.detectChanges();

    expect(component.getTrimmedMovieTitle()).toBe('abcdefghijklmnopqrstuvwxyz');
  });

  it('should navigate to omdb details', () => {
    component.omdb = true;
    fixture.detectChanges();

    fixture.debugElement.query(By.css('a')).nativeElement.click();
    expect(mockRouter.navigate).toHaveBeenCalledWith(['movie/id0000']);
  });

  it('should navigate to movie details', () => {
    component.omdb = false;
    fixture.detectChanges();

    fixture.debugElement.query(By.css('a')).nativeElement.click();
    expect(mockRouter.navigate).toHaveBeenCalledWith(['search/id0000']);
  });

  it('should display add button in omdb mode', () => {
    component.omdb = true;
    fixture.detectChanges();

    const button = fixture.debugElement.query(By.css('.btn-success'));
    expect(button).not.toBeNull();
  });

  it('should display add button in omdb mode', () => {
    component.omdb = false;
    fixture.detectChanges();

    const button = fixture.debugElement.query(By.css('.btn-success'));
    expect(button).toBeNull();
  });
});
