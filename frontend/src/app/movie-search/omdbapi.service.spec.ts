import { TestBed, getTestBed } from '@angular/core/testing';
import {
  HttpTestingController,
  HttpClientTestingModule,
} from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { OmdbapiService } from './omdbapi.service';
import { IOmdbMovie, IOmdbSearchResult } from './omdbapi.interface';
import { environment } from '../../environments/environment';

let injector: TestBed;
let OmdbAPIService: OmdbapiService;
let httpMock: HttpTestingController;

const baseUrl = environment.apiUrl;

beforeEach(() => {
  TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
    providers: [OmdbapiService],
  }).compileComponents();
});

describe('OmdbapiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OmdbapiService = TestBed.get(OmdbapiService);
    expect(service).toBeTruthy();
  });
});

describe('http service testing', () => {
  beforeEach(() => {
    injector = getTestBed();
    OmdbAPIService = injector.get(OmdbapiService);
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('searchMovie()', () => {
    const expectedOmdbResult: IOmdbSearchResult = {
      Search: [],
      totalResults: 92,
      Response: true,
    };

    OmdbAPIService.searchMovie('test', 1).subscribe(data => {
      expect(data.totalResults).toBe(92);
      expect(data.Response).toBeTruthy();
    });

    const req = httpMock.expectOne(baseUrl + '&s=test&page=1');
    expect(req.request.method).toEqual('GET');
    req.flush(expectedOmdbResult);
  });

  it('getMovie()', () => {
      const expectedOmdbMovie: IOmdbMovie = {
        Title: 'title',
        Year: '2019',
        Plot: 'lorem lipsum',
      };

      OmdbAPIService.getMovie('12345').subscribe(data => {
        expect(data).toEqual(expectedOmdbMovie);
          });

      const req = httpMock.expectOne(baseUrl + '&i=12345');
      expect(req.request.method).toEqual('GET');
      req.flush(expectedOmdbMovie);
      });
});
