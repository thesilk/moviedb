import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IOmdbMovie } from '../omdbapi.interface';
import { OmdbapiService } from '../omdbapi.service';

@Component({
  selector: 'app-search-item-view',
  templateUrl: './search-item-view.component.html',
  styleUrls: ['./search-item-view.component.scss']
})
export class SearchItemViewComponent implements OnInit {
  movie: IOmdbMovie;

  private id: string;

  constructor(private route: ActivatedRoute, private omdbapi: OmdbapiService) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');

    this.omdbapi.getMovie(this.id).subscribe(data => {
      this.movie = data;
      console.log(data);
    });
  }

}
