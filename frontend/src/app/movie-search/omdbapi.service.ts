import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { retry } from 'rxjs/operators';

import { IOmdbSearchResult, IOmdbMovie } from './omdbapi.interface';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class OmdbapiService {
  private api = environment.apiUrl;

  constructor(private http: HttpClient) {}

  searchMovie(movieName: string, page: number): Observable<IOmdbSearchResult> {
    return this.http
      .get<IOmdbSearchResult>(`${this.api}&s=${movieName}&page=${page}`)
      .pipe(retry(3));
  }

  getMovie(id: string): Observable<IOmdbMovie> {
    return this.http.get<IOmdbMovie>(`${this.api}&i=${id}`).pipe(retry(3));
  }

  checkImageURL(url: string): Observable<any> {
    return this.http.get<any>(url).pipe(retry(3));
  }
}
