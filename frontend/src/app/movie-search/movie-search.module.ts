import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SearchComponent } from './search/search.component';
import { OmdbapiService } from './omdbapi.service';
import { SearchItemViewComponent } from './search-item-view/search-item-view.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [SearchComponent, SearchItemViewComponent],
  imports: [CommonModule, RouterModule, SharedModule],
  providers: [OmdbapiService],
  exports: [SearchComponent],
})
export class MovieSearchModule {}
