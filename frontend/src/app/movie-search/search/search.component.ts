import { Component, OnInit } from '@angular/core';

import { OmdbapiService } from '../omdbapi.service';
import { IOmdbSearchResult } from '../omdbapi.interface';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  results: IOmdbSearchResult;
  page = 1;
  searchedMovie: string;

  constructor(private omdb: OmdbapiService) {}

  ngOnInit() {}

  searchMovie(value: string) {
    this.searchedMovie = value;
    this.omdb.searchMovie(value, this.page).subscribe(data => {
      this.results = data;
    });
  }

  nextPage(event) {
    if (this.page + 1 <= this.results.totalResults / 10) {
      this.page++;
      this.omdb.searchMovie(this.searchedMovie, this.page).subscribe(data => {
        this.results = data;
      });
    }
  }

  prevPage(event) {
    if (this.page - 1 >= 1) {
      this.page--;
      this.omdb.searchMovie(this.searchedMovie, this.page).subscribe(data => {
        this.results = data;
      });
    }
  }
}
